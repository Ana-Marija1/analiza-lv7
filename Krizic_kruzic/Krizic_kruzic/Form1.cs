﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Krizic_kruzic
{
    public partial class Form1 : Form
    {
        bool turn = true;
        int turnCount = 0;
        
        public Form1()
        {
            InitializeComponent();
            
        }

        private void izađiIzIgreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void grid_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = grid.CreateGraphics();
            Pen blackPen = new Pen(Color.Black, 1);

            g.DrawLine(blackPen, 10, 75, 210, 75);
            g.DrawLine(blackPen, 10, 150, 210, 150);
            g.DrawLine(blackPen, 75, 10, 75, 210);
            g.DrawLine(blackPen, 150, 10, 150, 210);
        }

        private void buttonClick(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (turn)
            {
                b.Text = "X";
            }
            else
            {
                b.Text = "O";
            }
            turn = !turn;
            b.Enabled = false;
            turnCount++;
            checkForWinner();
        }

        private void checkForWinner()
        {
            bool winner = false;
            if ((b11.Text == b12.Text) && (b12.Text == b13.Text) && (!b11.Enabled))
            {
                winner = true;
            }
            else if ((b21.Text == b22.Text) && (b22.Text == b23.Text) && (!b21.Enabled))
            {
                winner = true;
            }
            else if ((b31.Text == b32.Text) && (b32.Text == b33.Text) && (!b31.Enabled))
            {
                winner = true;
            }
            else if ((b11.Text == b21.Text) && (b21.Text == b31.Text) && (!b11.Enabled))
            {
                winner = true;
            }
            else if ((b12.Text == b22.Text) && (b22.Text == b32.Text) && (!b12.Enabled))
            {
                winner = true;
            }
            else if ((b13.Text == b23.Text) && (b23.Text == b33.Text) && (!b13.Enabled))
            {
                winner = true;
            }
            else if ((b11.Text == b22.Text) && (b22.Text == b33.Text) && (!b11.Enabled))
            {
                winner = true;
            }
            else if ((b13.Text == b22.Text) && (b22.Text == b31.Text) && (!b13.Enabled))
            {
                winner = true;
            }
            if (winner)
            {
                disableButtons();
                String isWinner = "";
                if (turn)
                {
                    
                    isWinner = tbPlayer2.Text;
                    lWinPlayer2.Text = (Int32.Parse(lWinPlayer2.Text) + 1).ToString();
                }
                else
                {
                    isWinner = tbPlayer1.Text;
                    lWinPlayer1.Text = (Int32.Parse(lWinPlayer1.Text) + 1).ToString();
                }
                MessageBox.Show(isWinner + " je pobjednik!");
            }
            else if (turnCount == 9)
            {
                lDraw.Text = (Int32.Parse(lDraw.Text) + 1).ToString();
                MessageBox.Show("Izjednačeno!");
            }

        }
        private void disableButtons()
        {
            b11.Enabled = false;
            b12.Enabled = false;
            b13.Enabled = false;
            b21.Enabled = false;
            b22.Enabled = false;
            b23.Enabled = false;
            b31.Enabled = false;
            b32.Enabled = false;
            b33.Enabled = false;
        }

        private void novaIgraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;
            turnCount = 0;
            b11.Text = "";
            b11.Enabled = true;
            b12.Text = "";
            b12.Enabled = true;
            b13.Text = "";
            b13.Enabled = true;
            b21.Text = "";
            b21.Enabled = true;
            b22.Text = "";
            b22.Enabled = true;
            b23.Text = "";
            b23.Enabled = true;
            b31.Text = "";
            b31.Enabled = true;
            b32.Text = "";
            b32.Enabled = true;
            b33.Text = "";
            b33.Enabled = true;
            
        }

        private void buttonEnter(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                if (turn)
                {
                    b.Text = "X";
                }
                else
                {
                    b.Text = "O";
                }
            }
        }

        private void buttonLeave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }
        }

       

        private void brisanjeRezultataPobjedaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lWinPlayer1.Text = "0";
            lWinPlayer2.Text = "0";
            lDraw.Text = "0";
        }
    }
}
