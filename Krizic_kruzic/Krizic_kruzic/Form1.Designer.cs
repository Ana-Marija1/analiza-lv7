﻿namespace Krizic_kruzic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novaIgraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izađiIzIgreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.b11 = new System.Windows.Forms.Button();
            this.b12 = new System.Windows.Forms.Button();
            this.b13 = new System.Windows.Forms.Button();
            this.b21 = new System.Windows.Forms.Button();
            this.b22 = new System.Windows.Forms.Button();
            this.b23 = new System.Windows.Forms.Button();
            this.b31 = new System.Windows.Forms.Button();
            this.b32 = new System.Windows.Forms.Button();
            this.b33 = new System.Windows.Forms.Button();
            this.grid = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbPlayer1 = new System.Windows.Forms.TextBox();
            this.tbPlayer2 = new System.Windows.Forms.TextBox();
            this.lWinPlayer1 = new System.Windows.Forms.Label();
            this.lDraw = new System.Windows.Forms.Label();
            this.lWinPlayer2 = new System.Windows.Forms.Label();
            this.brisanjeRezultataPobjedaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.grid.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(591, 30);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaIgraToolStripMenuItem,
            this.brisanjeRezultataPobjedaToolStripMenuItem,
            this.izađiIzIgreToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // novaIgraToolStripMenuItem
            // 
            this.novaIgraToolStripMenuItem.Name = "novaIgraToolStripMenuItem";
            this.novaIgraToolStripMenuItem.Size = new System.Drawing.Size(266, 26);
            this.novaIgraToolStripMenuItem.Text = "Nova Igra";
            this.novaIgraToolStripMenuItem.Click += new System.EventHandler(this.novaIgraToolStripMenuItem_Click);
            // 
            // izađiIzIgreToolStripMenuItem
            // 
            this.izađiIzIgreToolStripMenuItem.Name = "izađiIzIgreToolStripMenuItem";
            this.izađiIzIgreToolStripMenuItem.Size = new System.Drawing.Size(266, 26);
            this.izađiIzIgreToolStripMenuItem.Text = "Izađi iz Igre";
            this.izađiIzIgreToolStripMenuItem.Click += new System.EventHandler(this.izađiIzIgreToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // b11
            // 
            this.b11.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b11.Location = new System.Drawing.Point(20, 13);
            this.b11.Name = "b11";
            this.b11.Size = new System.Drawing.Size(70, 70);
            this.b11.TabIndex = 2;
            this.b11.UseVisualStyleBackColor = true;
            this.b11.Click += new System.EventHandler(this.buttonClick);
            this.b11.MouseEnter += new System.EventHandler(this.buttonEnter);
            this.b11.MouseLeave += new System.EventHandler(this.buttonLeave);
            // 
            // b12
            // 
            this.b12.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b12.Location = new System.Drawing.Point(117, 13);
            this.b12.Name = "b12";
            this.b12.Size = new System.Drawing.Size(70, 70);
            this.b12.TabIndex = 3;
            this.b12.UseVisualStyleBackColor = true;
            this.b12.Click += new System.EventHandler(this.buttonClick);
            this.b12.MouseEnter += new System.EventHandler(this.buttonEnter);
            this.b12.MouseLeave += new System.EventHandler(this.buttonLeave);
            // 
            // b13
            // 
            this.b13.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b13.Location = new System.Drawing.Point(210, 13);
            this.b13.Name = "b13";
            this.b13.Size = new System.Drawing.Size(70, 70);
            this.b13.TabIndex = 4;
            this.b13.UseVisualStyleBackColor = true;
            this.b13.Click += new System.EventHandler(this.buttonClick);
            this.b13.MouseEnter += new System.EventHandler(this.buttonEnter);
            this.b13.MouseLeave += new System.EventHandler(this.buttonLeave);
            // 
            // b21
            // 
            this.b21.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b21.Location = new System.Drawing.Point(20, 103);
            this.b21.Name = "b21";
            this.b21.Size = new System.Drawing.Size(70, 70);
            this.b21.TabIndex = 5;
            this.b21.UseVisualStyleBackColor = true;
            this.b21.Click += new System.EventHandler(this.buttonClick);
            this.b21.MouseEnter += new System.EventHandler(this.buttonEnter);
            this.b21.MouseLeave += new System.EventHandler(this.buttonLeave);
            // 
            // b22
            // 
            this.b22.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b22.Location = new System.Drawing.Point(117, 103);
            this.b22.Name = "b22";
            this.b22.Size = new System.Drawing.Size(70, 70);
            this.b22.TabIndex = 6;
            this.b22.UseVisualStyleBackColor = true;
            this.b22.Click += new System.EventHandler(this.buttonClick);
            this.b22.MouseEnter += new System.EventHandler(this.buttonEnter);
            this.b22.MouseLeave += new System.EventHandler(this.buttonLeave);
            // 
            // b23
            // 
            this.b23.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b23.Location = new System.Drawing.Point(210, 103);
            this.b23.Name = "b23";
            this.b23.Size = new System.Drawing.Size(70, 70);
            this.b23.TabIndex = 7;
            this.b23.UseVisualStyleBackColor = true;
            this.b23.Click += new System.EventHandler(this.buttonClick);
            this.b23.MouseEnter += new System.EventHandler(this.buttonEnter);
            this.b23.MouseLeave += new System.EventHandler(this.buttonLeave);
            // 
            // b31
            // 
            this.b31.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b31.Location = new System.Drawing.Point(20, 194);
            this.b31.Name = "b31";
            this.b31.Size = new System.Drawing.Size(70, 70);
            this.b31.TabIndex = 8;
            this.b31.UseVisualStyleBackColor = true;
            this.b31.Click += new System.EventHandler(this.buttonClick);
            this.b31.MouseEnter += new System.EventHandler(this.buttonEnter);
            this.b31.MouseLeave += new System.EventHandler(this.buttonLeave);
            // 
            // b32
            // 
            this.b32.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b32.Location = new System.Drawing.Point(117, 194);
            this.b32.Name = "b32";
            this.b32.Size = new System.Drawing.Size(70, 70);
            this.b32.TabIndex = 9;
            this.b32.UseVisualStyleBackColor = true;
            this.b32.Click += new System.EventHandler(this.buttonClick);
            this.b32.MouseEnter += new System.EventHandler(this.buttonEnter);
            this.b32.MouseLeave += new System.EventHandler(this.buttonLeave);
            // 
            // b33
            // 
            this.b33.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.b33.Location = new System.Drawing.Point(210, 194);
            this.b33.Name = "b33";
            this.b33.Size = new System.Drawing.Size(70, 70);
            this.b33.TabIndex = 10;
            this.b33.UseVisualStyleBackColor = true;
            this.b33.Click += new System.EventHandler(this.buttonClick);
            this.b33.MouseEnter += new System.EventHandler(this.buttonEnter);
            this.b33.MouseLeave += new System.EventHandler(this.buttonLeave);
            // 
            // grid
            // 
            this.grid.Controls.Add(this.b11);
            this.grid.Controls.Add(this.b12);
            this.grid.Controls.Add(this.b33);
            this.grid.Controls.Add(this.b13);
            this.grid.Controls.Add(this.b32);
            this.grid.Controls.Add(this.b21);
            this.grid.Controls.Add(this.b31);
            this.grid.Controls.Add(this.b22);
            this.grid.Controls.Add(this.b23);
            this.grid.Location = new System.Drawing.Point(12, 31);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(305, 289);
            this.grid.TabIndex = 11;
            this.grid.Paint += new System.Windows.Forms.PaintEventHandler(this.grid_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(338, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Križić";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(380, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "Broj izjednačednih igara";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(338, 225);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Kružić";
            // 
            // tbPlayer1
            // 
            this.tbPlayer1.Location = new System.Drawing.Point(399, 41);
            this.tbPlayer1.MaxLength = 20;
            this.tbPlayer1.Name = "tbPlayer1";
            this.tbPlayer1.Size = new System.Drawing.Size(145, 22);
            this.tbPlayer1.TabIndex = 15;
            this.tbPlayer1.Text = "Igrač 1";
            // 
            // tbPlayer2
            // 
            this.tbPlayer2.Location = new System.Drawing.Point(399, 222);
            this.tbPlayer2.MaxLength = 20;
            this.tbPlayer2.Name = "tbPlayer2";
            this.tbPlayer2.Size = new System.Drawing.Size(145, 22);
            this.tbPlayer2.TabIndex = 16;
            this.tbPlayer2.Text = "Igrač 2";
            // 
            // lWinPlayer1
            // 
            this.lWinPlayer1.AutoSize = true;
            this.lWinPlayer1.Location = new System.Drawing.Point(550, 44);
            this.lWinPlayer1.Name = "lWinPlayer1";
            this.lWinPlayer1.Size = new System.Drawing.Size(16, 17);
            this.lWinPlayer1.TabIndex = 17;
            this.lWinPlayer1.Text = "0";
            this.lWinPlayer1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lDraw
            // 
            this.lDraw.AutoSize = true;
            this.lDraw.Location = new System.Drawing.Point(550, 134);
            this.lDraw.Name = "lDraw";
            this.lDraw.Size = new System.Drawing.Size(16, 17);
            this.lDraw.TabIndex = 18;
            this.lDraw.Text = "0";
            // 
            // lWinPlayer2
            // 
            this.lWinPlayer2.AutoSize = true;
            this.lWinPlayer2.Location = new System.Drawing.Point(550, 225);
            this.lWinPlayer2.Name = "lWinPlayer2";
            this.lWinPlayer2.Size = new System.Drawing.Size(16, 17);
            this.lWinPlayer2.TabIndex = 19;
            this.lWinPlayer2.Text = "0";
            // 
            // brisanjeRezultataPobjedaToolStripMenuItem
            // 
            this.brisanjeRezultataPobjedaToolStripMenuItem.Name = "brisanjeRezultataPobjedaToolStripMenuItem";
            this.brisanjeRezultataPobjedaToolStripMenuItem.Size = new System.Drawing.Size(266, 26);
            this.brisanjeRezultataPobjedaToolStripMenuItem.Text = "Brisanje rezultata pobjeda";
            this.brisanjeRezultataPobjedaToolStripMenuItem.Click += new System.EventHandler(this.brisanjeRezultataPobjedaToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 344);
            this.Controls.Add(this.lWinPlayer2);
            this.Controls.Add(this.lDraw);
            this.Controls.Add(this.lWinPlayer1);
            this.Controls.Add(this.tbPlayer2);
            this.Controls.Add(this.tbPlayer1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.grid);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Križić-Kružić";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grid.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novaIgraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izađiIzIgreToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button b11;
        private System.Windows.Forms.Button b12;
        private System.Windows.Forms.Button b13;
        private System.Windows.Forms.Button b21;
        private System.Windows.Forms.Button b22;
        private System.Windows.Forms.Button b23;
        private System.Windows.Forms.Button b31;
        private System.Windows.Forms.Button b32;
        private System.Windows.Forms.Button b33;
        private System.Windows.Forms.Panel grid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbPlayer1;
        private System.Windows.Forms.TextBox tbPlayer2;
        private System.Windows.Forms.Label lWinPlayer1;
        private System.Windows.Forms.Label lDraw;
        private System.Windows.Forms.Label lWinPlayer2;
        private System.Windows.Forms.ToolStripMenuItem brisanjeRezultataPobjedaToolStripMenuItem;
    }
}

